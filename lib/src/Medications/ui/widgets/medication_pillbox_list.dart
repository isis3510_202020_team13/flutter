import 'package:f412/src/Medications/ui/screens/medication_pillbox.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MedicationPillboxList extends StatelessWidget{


  @override
  Widget build(BuildContext context) {

    String name1 = "Clonazepam";
    int dose1 = 30;
    String unit1 = "mg";

    String name2 = "Rivotril";
    int dose2 = 60;
    String unit2 = "mg";

    String name3 = "Bupropion";
    int dose3 = 40;
    String unit3 = "mg";

    String name4 = "Quetiapine";
    int dose4 = 35;
    String unit4 = "mg";

    double thick = 1.0;

    final medicationList = ListView(
      children: [
        Container(
          margin: EdgeInsets.only(
            top: 10.0,
            left: 10.0,
            bottom: 10.0
          ),
          child: Text(
            "Today's medications:",
            style: TextStyle(
              color: Colors.deepOrangeAccent,
              fontSize: 25.0
            ),

          ),
        ),
        MedicationPillbox(name1, dose1, unit1),
        Divider(
          thickness: thick,
        ),
        MedicationPillbox(name2, dose2, unit2),
        Divider(
          thickness: thick,
        ),
        MedicationPillbox(name3, dose3, unit3),
        Divider(
          thickness: thick,
        ),
        MedicationPillbox(name4, dose4, unit4),
        Divider(
          thickness: thick,
        ),
        MedicationPillbox(name1, dose1, unit1),
        Divider(
          thickness: thick,
        ),
        MedicationPillbox(name2, dose2, unit2),
        Divider(
          thickness: thick,
        ),
        MedicationPillbox(name3, dose3, unit3),
        Divider(
          thickness: thick,
        ),
        MedicationPillbox(name4, dose4, unit4),
      ],

    );

    return medicationList;




  }





}