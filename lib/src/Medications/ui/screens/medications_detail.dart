import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class MedicationsDetail extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return  Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: <Widget> [

        ],
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.deepOrangeAccent,
        ),
        title: Text(
          "",
          style: TextStyle(
            color: Colors.blueAccent,
          ),
        ),
      ),
      body: Container(
        margin: EdgeInsets.only(
            top: 10.0,
            left: 10.0,
            right: 10.0,
            bottom: 10.0
        ),
        child: ListView(
          children: [
            Container(
              margin: EdgeInsets.all(10.0),
              child: Text(
                "Acamprosate",
                style: TextStyle(
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.deepOrangeAccent
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.all(10.0),
              child: Text(
                "Acamprosate is thought to stabilize chemical signaling in the brain that would otherwise be disrupted by alcohol withdrawal.[3] When used alone, acamprosate is not an effective therapy for alcoholism in most individuals;[4] studies have found that acamprosate works best when used in combination with psychosocial support since it facilitates a reduction in alcohol consumption as well as full abstinence",
                style: TextStyle(
                  fontSize: 20.0,
                ),
                textAlign: TextAlign.justify,
              ),
            ),
            Container(
              margin: EdgeInsets.all(10.0),
              child: Text(
                "Adverse Effects",
                style: TextStyle(
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.deepOrangeAccent
                ),
              ),
            ),
            Container(
              margin: EdgeInsets.all(10.0),
              child: Text(
               "The US label carries warnings about increases of suicidal behavior, major depressive disorder, and kidney failure"

                "Adverse effects that caused people to stop taking the drug in clinical trials included diarrhea, nausea, depression, and anxiety"

                "Potential adverse effects include headache, stomach pain, back pain, muscle pain, joint pain, chest pain, infections, flu-like symptoms, chills, heart palpitations, high blood pressure, fainting, vomiting, upset stomach, constipation, increased appetite, weight gain, edema, sleepiness, decreased sex drive, impotence, forgetfulness, abnormal thinking, abnormal vision, distorted sense of taste, tremors, runny nose, coughing, difficulty breathing, sore throat, bronchitis, and rashes",
                style: TextStyle(
                  fontSize: 20.0,
                ),
                textAlign: TextAlign.justify,
              ),
            ),
          ],
        ),
      ),
    );
  }

}