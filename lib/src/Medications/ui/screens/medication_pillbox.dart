import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class MedicationPillbox extends StatelessWidget{

  String medicationName = "";
  int dose = 0;
  String unit = "";
  int frequency = 0;
  String frequencyPer = "";
  int dispenseAmount = 0;
  List daysOfTheWeek = [];

  MedicationPillbox(this.medicationName,this.dose, this.unit);


  @override
  Widget build(BuildContext context) {


    final medicationPillbox = Container(
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(
              left: 20.0,
              top: 10.0
            ),
            child: Icon(
              FontAwesomeIcons.pills,
              color: Colors.deepOrangeAccent,
              size: 18.0,
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              left: 20.0,
              top: 10.0
            ),
            child: Text(
                medicationName + " " + dose.toString() + unit,
              style: TextStyle(fontSize: 18.0),
            ),
          )
        ],
      ),
    );


    return medicationPillbox;
  }
}