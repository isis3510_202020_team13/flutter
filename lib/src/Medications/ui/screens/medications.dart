import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'medications_detail.dart';

class Medications extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
   return Scaffold(
       backgroundColor: Colors.white,
       appBar: AppBar(
         actions: <Widget> [

         ],
         backgroundColor: Colors.white,
         iconTheme: IconThemeData(
           color: Colors.deepOrangeAccent,
         ),
         title: Text(
           "Medications",
           style: TextStyle(
             color: Colors.deepOrangeAccent,
           ),
         ),
       ),
       body: ListView(
         children: [
           InkWell(
             onTap: (){
               Navigator.push(context, MaterialPageRoute(builder: (context) {
                 return MedicationsDetail();
               }));
             },
             child: Container(
               margin: EdgeInsets.only(
                   top: 10.0,
                   left: 10.0
               ),
               child: Row(
                 mainAxisAlignment: MainAxisAlignment.center,
                 children: [
                   Icon(FontAwesomeIcons.pills, color: Colors.deepOrangeAccent,),
                   Expanded(
                     child: Container(
                         margin: EdgeInsets.only(
                             left: 20.0
                         ),
                         child: Text("Acamprosate", style: TextStyle(fontSize: 20.0),)
                     ),
                   )
                 ],
               ),
             ),
           ),
           Divider(),
           Container(
             margin: EdgeInsets.only(
                 left: 10.0
             ),
             child: Row(
               mainAxisAlignment: MainAxisAlignment.center,
               children: [
                 Icon(FontAwesomeIcons.pills, color: Colors.deepOrangeAccent,),
                 Expanded(
                   child: Container(
                       margin: EdgeInsets.only(
                           left: 20.0
                       ),
                       child: Text(" Alprazolam", style: TextStyle(fontSize: 20.0),)
                   ),
                 )
               ],
             ),
           ),
           Divider(),
           Container(
             margin: EdgeInsets.only(
                 left: 10.0
             ),
             child: Row(
               mainAxisAlignment: MainAxisAlignment.center,
               children: [
                 Icon(FontAwesomeIcons.pills, color: Colors.deepOrangeAccent,),
                 Expanded(
                   child: Container(
                       margin: EdgeInsets.only(
                           left: 20.0
                       ),
                       child: Text("Aripiprazole", style: TextStyle(fontSize: 20.0),)
                   ),
                 )
               ],
             ),
           ),
           Divider(),
           Container(
             margin: EdgeInsets.only(
                 left: 10.0
             ),
             child: Row(
               mainAxisAlignment: MainAxisAlignment.center,
               children: [
                 Icon(FontAwesomeIcons.pills, color: Colors.deepOrangeAccent,),
                 Expanded(
                   child: Container(
                       margin: EdgeInsets.only(
                           left: 20.0
                       ),
                       child: Text(" Buspirone", style: TextStyle(fontSize: 20.0),)
                   ),
                 )
               ],
             ),
           ),
           Divider(),
           Container(
             margin: EdgeInsets.only(
                 left: 10.0
             ),
             child: Row(
               mainAxisAlignment: MainAxisAlignment.center,
               children: [
                 Icon(FontAwesomeIcons.pills, color: Colors.deepOrangeAccent,),
                 Expanded(
                   child: Container(
                       margin: EdgeInsets.only(
                           left: 20.0
                       ),
                       child: Text("Bupropion", style: TextStyle(fontSize: 20.0),)
                   ),
                 )
               ],
             ),
           ),
           Divider(),
           Container(
             margin: EdgeInsets.only(
                 left: 10.0
             ),
             child: Row(
               mainAxisAlignment: MainAxisAlignment.center,
               children: [
                 Icon(FontAwesomeIcons.pills, color: Colors.deepOrangeAccent,),
                 Expanded(
                   child: Container(
                       margin: EdgeInsets.only(
                           left: 20.0
                       ),
                       child: Text("Duloxetine", style: TextStyle(fontSize: 20.0),)
                   ),
                 )
               ],
             ),
           ),
           Divider(),
           Container(
             margin: EdgeInsets.only(
                 left: 10.0
             ),
             child: Row(
               mainAxisAlignment: MainAxisAlignment.center,
               children: [
                 Icon(FontAwesomeIcons.pills, color: Colors.deepOrangeAccent,),
                 Expanded(
                   child: Container(
                       margin: EdgeInsets.only(
                           left: 20.0
                       ),
                       child: Text("Desvenlafaxine", style: TextStyle(fontSize: 20.0),)
                   ),
                 )
               ],
             ),
           ),
           Divider(),
           Container(
             margin: EdgeInsets.only(
                 top: 10.0,
                 left: 10.0
             ),
             child: Row(
               mainAxisAlignment: MainAxisAlignment.center,
               children: [
                 Icon(FontAwesomeIcons.pills, color: Colors.deepOrangeAccent,),
                 Expanded(
                   child: Container(
                       margin: EdgeInsets.only(
                           left: 20.0
                       ),
                       child: Text("Eszopiclone", style: TextStyle(fontSize: 20.0),)
                   ),
                 )
               ],
             ),
           ),
           Divider(),
           Container(
             margin: EdgeInsets.only(
                 left: 10.0
             ),
             child: Row(
               mainAxisAlignment: MainAxisAlignment.center,
               children: [
                 Icon(FontAwesomeIcons.pills, color: Colors.deepOrangeAccent,),
                 Expanded(
                   child: Container(
                       margin: EdgeInsets.only(
                           left: 20.0
                       ),
                       child: Text(" Alprazolam", style: TextStyle(fontSize: 20.0),)
                   ),
                 )
               ],
             ),
           ),
           Divider(),
           Container(
             margin: EdgeInsets.only(
                 left: 10.0
             ),
             child: Row(
               mainAxisAlignment: MainAxisAlignment.center,
               children: [
                 Icon(FontAwesomeIcons.pills, color: Colors.deepOrangeAccent,),
                 Expanded(
                   child: Container(
                       margin: EdgeInsets.only(
                           left: 20.0
                       ),
                       child: Text("Aripiprazole", style: TextStyle(fontSize: 20.0),)
                   ),
                 )
               ],
             ),
           ),
           Divider(),
           Container(
             margin: EdgeInsets.only(
                 left: 10.0
             ),
             child: Row(
               mainAxisAlignment: MainAxisAlignment.center,
               children: [
                 Icon(FontAwesomeIcons.pills, color: Colors.deepOrangeAccent,),
                 Expanded(
                   child: Container(
                       margin: EdgeInsets.only(
                           left: 20.0
                       ),
                       child: Text(" Buspirone", style: TextStyle(fontSize: 20.0),)
                   ),
                 )
               ],
             ),
           ),
           Divider(),
           Container(
             margin: EdgeInsets.only(
                 left: 10.0
             ),
             child: Row(
               mainAxisAlignment: MainAxisAlignment.center,
               children: [
                 Icon(FontAwesomeIcons.pills, color: Colors.deepOrangeAccent,),
                 Expanded(
                   child: Container(
                       margin: EdgeInsets.only(
                           left: 20.0
                       ),
                       child: Text("Bupropion", style: TextStyle(fontSize: 20.0),)
                   ),
                 )
               ],
             ),
           ),
           Divider(),
           Container(
             margin: EdgeInsets.only(
                 left: 10.0
             ),
             child: Row(
               mainAxisAlignment: MainAxisAlignment.center,
               children: [
                 Icon(FontAwesomeIcons.pills, color: Colors.deepOrangeAccent,),
                 Expanded(
                   child: Container(
                       margin: EdgeInsets.only(
                           left: 20.0
                       ),
                       child: Text("Duloxetine", style: TextStyle(fontSize: 20.0),)
                   ),
                 )
               ],
             ),
           ),
           Divider(),
           Container(
             margin: EdgeInsets.only(
                 left: 10.0
             ),
             child: Row(
               mainAxisAlignment: MainAxisAlignment.center,
               children: [
                 Icon(FontAwesomeIcons.pills, color: Colors.deepOrangeAccent,),
                 Expanded(
                   child: Container(
                       margin: EdgeInsets.only(
                           left: 20.0
                       ),
                       child: Text("Desvenlafaxine", style: TextStyle(fontSize: 20.0),)
                   ),
                 )
               ],
             ),
           ),
           Divider(),
           Container(
             margin: EdgeInsets.only(
                 top: 10.0,
                 left: 10.0
             ),
             child: Row(
               mainAxisAlignment: MainAxisAlignment.center,
               children: [
                 Icon(FontAwesomeIcons.pills, color: Colors.deepOrangeAccent,),
                 Expanded(
                   child: Container(
                       margin: EdgeInsets.only(
                           left: 20.0
                       ),
                       child: Text("Eszopiclone", style: TextStyle(fontSize: 20.0),)
                   ),
                 )
               ],
             ),
           ),
           Divider(),
           Container(
             margin: EdgeInsets.only(
                 left: 10.0
             ),
             child: Row(
               mainAxisAlignment: MainAxisAlignment.center,
               children: [
                 Icon(FontAwesomeIcons.pills, color: Colors.deepOrangeAccent,),
                 Expanded(
                   child: Container(
                       margin: EdgeInsets.only(
                           left: 20.0
                       ),
                       child: Text(" Alprazolam", style: TextStyle(fontSize: 20.0),)
                   ),
                 )
               ],
             ),
           ),
           Divider(),
           Container(
             margin: EdgeInsets.only(
                 left: 10.0
             ),
             child: Row(
               mainAxisAlignment: MainAxisAlignment.center,
               children: [
                 Icon(FontAwesomeIcons.pills, color: Colors.deepOrangeAccent,),
                 Expanded(
                   child: Container(
                       margin: EdgeInsets.only(
                           left: 20.0
                       ),
                       child: Text("Aripiprazole", style: TextStyle(fontSize: 20.0),)
                   ),
                 )
               ],
             ),
           ),
           Divider(),
           Container(
             margin: EdgeInsets.only(
                 left: 10.0
             ),
             child: Row(
               mainAxisAlignment: MainAxisAlignment.center,
               children: [
                 Icon(FontAwesomeIcons.pills, color: Colors.deepOrangeAccent,),
                 Expanded(
                   child: Container(
                       margin: EdgeInsets.only(
                           left: 20.0
                       ),
                       child: Text(" Buspirone", style: TextStyle(fontSize: 20.0),)
                   ),
                 )
               ],
             ),
           ),
           Divider(),
           Container(
             margin: EdgeInsets.only(
                 left: 10.0
             ),
             child: Row(
               mainAxisAlignment: MainAxisAlignment.center,
               children: [
                 Icon(FontAwesomeIcons.pills, color: Colors.deepOrangeAccent,),
                 Expanded(
                   child: Container(
                       margin: EdgeInsets.only(
                           left: 20.0
                       ),
                       child: Text("Bupropion", style: TextStyle(fontSize: 20.0),)
                   ),
                 )
               ],
             ),
           ),
           Divider(),
           Container(
             margin: EdgeInsets.only(
                 left: 10.0
             ),
             child: Row(
               mainAxisAlignment: MainAxisAlignment.center,
               children: [
                 Icon(FontAwesomeIcons.pills, color: Colors.deepOrangeAccent,),
                 Expanded(
                   child: Container(
                       margin: EdgeInsets.only(
                           left: 20.0
                       ),
                       child: Text("Duloxetine", style: TextStyle(fontSize: 20.0),)
                   ),
                 )
               ],
             ),
           ),
           Divider(),
           Container(
             margin: EdgeInsets.only(
                 left: 10.0
             ),
             child: Row(
               mainAxisAlignment: MainAxisAlignment.center,
               children: [
                 Icon(FontAwesomeIcons.pills, color: Colors.deepOrangeAccent,),
                 Expanded(
                   child: Container(
                       margin: EdgeInsets.only(
                           left: 20.0
                       ),
                       child: Text("Desvenlafaxine", style: TextStyle(fontSize: 20.0),)
                   ),
                 )
               ],
             ),
           ),
           Divider(),
           Container(
             margin: EdgeInsets.only(
                 top: 10.0,
                 left: 10.0
             ),
             child: Row(
               mainAxisAlignment: MainAxisAlignment.center,
               children: [
                 Icon(FontAwesomeIcons.pills, color: Colors.deepOrangeAccent,),
                 Expanded(
                   child: Container(
                       margin: EdgeInsets.only(
                           left: 20.0
                       ),
                       child: Text("Eszopiclone", style: TextStyle(fontSize: 20.0),)
                   ),
                 )
               ],
             ),
           ),

         ],
       )
   );
  }

}