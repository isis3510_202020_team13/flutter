import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class DisordersDetail extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return  Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          actions: <Widget> [

          ],
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.blueAccent,
          ),
          title: Text(
            "",
            style: TextStyle(
              color: Colors.blueAccent,
            ),
          ),
        ),
        body: Container(
          margin: EdgeInsets.only(
            top: 10.0,
            left: 10.0,
            right: 10.0,
            bottom: 10.0
          ),
          child: ListView(
            children: [
              Container(
                margin: EdgeInsets.all(10.0),
                child: Text(
                  "Disruptive Mood Dysregulation Disorder",
                  style: TextStyle(
                    fontSize: 30.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.blueAccent
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(10.0),
                child: Text(
                    "Disruptive mood dysregulation disorder (DMDD) is a condition in which children or adolescents experience ongoing irritability, anger, and frequent, intense temper outbursts. The symptoms of DMDD go beyond a “bad mood.” DMDD symptoms are severe. Youth who have DMDD experience significant problems at home, at school, and often with peers.",
                  style: TextStyle(
                    fontSize: 20.0,
                  ),
                  textAlign: TextAlign.justify,
                ),
              ),
              Container(
                margin: EdgeInsets.all(10.0),
                child: Text(
                  "Symptoms",
                  style: TextStyle(
                      fontSize: 30.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.blueAccent
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(10.0),
                child: Text(
                  "Severe temper outbursts (verbal or behavioral), on average, three or more times per week"
                    "Outbursts and tantrums that have been ongoing for at least 12 months"
                "Chronically irritable or angry mood most of the day, nearly every day"
                "Trouble functioning due to irritability in more than one place (at home, at school, and with peers)",
                  style: TextStyle(
                    fontSize: 20.0,
                  ),
                  textAlign: TextAlign.justify,
                ),
              ),
              Container(
                margin: EdgeInsets.all(10.0),
                child: Text(
                  "Treatment",
                  style: TextStyle(
                      fontSize: 30.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.blueAccent
                  ),
                ),
              ),
              Container(
                margin: EdgeInsets.all(10.0),
                child: Text(
                 "DMDD is a newly classified disorder, and few DMDD-specific treatment studies have been conducted to date. Current treatments are primarily based on research focused on other childhood disorders associated with irritability (such as anxiety and ADHD). Fortunately, many of these treatments also work for DMDD. NIMH is currently funding studies focused on further improving these treatments and identifying new treatments specifically for DMDD. It is important for parents or caregivers to work closely with their child’s doctor to make treatment decisions that are best for their child.",
                  style: TextStyle(
                    fontSize: 20.0,
                  ),
                  textAlign: TextAlign.justify,
                ),
              ),

            ],
          ),
        ),
    );
  }


}