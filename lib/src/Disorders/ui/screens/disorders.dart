import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

import 'disorders_detail.dart';

class Disorders extends StatelessWidget{
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return  Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          actions: <Widget> [

          ],
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.blueAccent,
          ),
          title: Text(
            "Disorders",
            style: TextStyle(
              color: Colors.blueAccent,
            ),
          ),
        ),
        body: ListView(
          children: [
            InkWell(
              onTap: (){
                Navigator.push(context, MaterialPageRoute(builder: (context) {
                  return DisordersDetail();
                }));
              },
              child: Container(
                margin: EdgeInsets.only(
                  top: 10.0,
                  left: 10.0
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Icon(FontAwesomeIcons.pills, color: Colors.blueAccent,),
                    Expanded(
                      child: Container(
                        margin: EdgeInsets.only(
                          left: 20.0
                        ),
                          child: Text("Disruptive Mood Dysregulation Disorder", style: TextStyle(fontSize: 20.0),)
                      ),
                    )
                  ],
                ),
              ),
            ),
            Divider(),
            Container(
              margin: EdgeInsets.only(
                  left: 10.0
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(FontAwesomeIcons.pills, color: Colors.blueAccent,),
                  Expanded(
                    child: Container(
                        margin: EdgeInsets.only(
                            left: 20.0
                        ),
                        child: Text("Major Depressive Disorder", style: TextStyle(fontSize: 20.0),)
                    ),
                  )
                ],
              ),
            ),
            Divider(),
            Container(
              margin: EdgeInsets.only(
                  left: 10.0
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(FontAwesomeIcons.pills, color: Colors.blueAccent,),
                  Expanded(
                    child: Container(
                        margin: EdgeInsets.only(
                            left: 20.0
                        ),
                        child: Text("Premenstrual Dysphoric Disorder", style: TextStyle(fontSize: 20.0),)
                    ),
                  )
                ],
              ),
            ),
            Divider(),
            Container(
              margin: EdgeInsets.only(
                  left: 10.0
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(FontAwesomeIcons.pills, color: Colors.blueAccent,),
                  Expanded(
                    child: Container(
                        margin: EdgeInsets.only(
                            left: 20.0
                        ),
                        child: Text("Cyclothymic Disorder", style: TextStyle(fontSize: 20.0),)
                    ),
                  )
                ],
              ),
            ),
            Divider(),
            Container(
              margin: EdgeInsets.only(
                  left: 10.0
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(FontAwesomeIcons.pills, color: Colors.blueAccent,),
                  Expanded(
                    child: Container(
                        margin: EdgeInsets.only(
                            left: 20.0
                        ),
                        child: Text("Panic Disorder", style: TextStyle(fontSize: 20.0),)
                    ),
                  )
                ],
              ),
            ),
            Divider(),
            Container(
              margin: EdgeInsets.only(
                  left: 10.0
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(FontAwesomeIcons.pills, color: Colors.blueAccent,),
                  Expanded(
                    child: Container(
                        margin: EdgeInsets.only(
                            left: 20.0
                        ),
                        child: Text("Agoraphobia", style: TextStyle(fontSize: 20.0),)
                    ),
                  )
                ],
              ),
            ),
            Divider(),
            Container(
              margin: EdgeInsets.only(
                  left: 10.0
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(FontAwesomeIcons.pills, color: Colors.blueAccent,),
                  Expanded(
                    child: Container(
                        margin: EdgeInsets.only(
                            left: 20.0
                        ),
                        child: Text("Obsessive-Compulsive Disorder", style: TextStyle(fontSize: 20.0),)
                    ),
                  )
                ],
              ),
            ),
            Divider(),
            Container(
              margin: EdgeInsets.only(
                  top: 10.0,
                  left: 10.0
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(FontAwesomeIcons.pills, color: Colors.blueAccent,),
                  Expanded(
                    child: Container(
                        margin: EdgeInsets.only(
                            left: 20.0
                        ),
                        child: Text("Disruptive Mood Dysregulation Disorder", style: TextStyle(fontSize: 20.0),)
                    ),
                  )
                ],
              ),
            ),
            Divider(),
            Container(
              margin: EdgeInsets.only(
                  left: 10.0
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(FontAwesomeIcons.pills, color: Colors.blueAccent,),
                  Expanded(
                    child: Container(
                        margin: EdgeInsets.only(
                            left: 20.0
                        ),
                        child: Text("Major Depressive Disorder", style: TextStyle(fontSize: 20.0),)
                    ),
                  )
                ],
              ),
            ),
            Divider(),
            Container(
              margin: EdgeInsets.only(
                  left: 10.0
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(FontAwesomeIcons.pills, color: Colors.blueAccent,),
                  Expanded(
                    child: Container(
                        margin: EdgeInsets.only(
                            left: 20.0
                        ),
                        child: Text("Premenstrual Dysphoric Disorder", style: TextStyle(fontSize: 20.0),)
                    ),
                  )
                ],
              ),
            ),
            Divider(),
            Container(
              margin: EdgeInsets.only(
                  left: 10.0
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(FontAwesomeIcons.pills, color: Colors.blueAccent,),
                  Expanded(
                    child: Container(
                        margin: EdgeInsets.only(
                            left: 20.0
                        ),
                        child: Text("Cyclothymic Disorder", style: TextStyle(fontSize: 20.0),)
                    ),
                  )
                ],
              ),
            ),
            Divider(),
            Container(
              margin: EdgeInsets.only(
                  left: 10.0
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(FontAwesomeIcons.pills, color: Colors.blueAccent,),
                  Expanded(
                    child: Container(
                        margin: EdgeInsets.only(
                            left: 20.0
                        ),
                        child: Text("Panic Disorder", style: TextStyle(fontSize: 20.0),)
                    ),
                  )
                ],
              ),
            ),
            Divider(),
            Container(
              margin: EdgeInsets.only(
                  left: 10.0
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(FontAwesomeIcons.pills, color: Colors.blueAccent,),
                  Expanded(
                    child: Container(
                        margin: EdgeInsets.only(
                            left: 20.0
                        ),
                        child: Text("Agoraphobia", style: TextStyle(fontSize: 20.0),)
                    ),
                  )
                ],
              ),
            ),
            Divider(),
            Container(
              margin: EdgeInsets.only(
                  left: 10.0
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(FontAwesomeIcons.pills, color: Colors.blueAccent,),
                  Expanded(
                    child: Container(
                        margin: EdgeInsets.only(
                            left: 20.0
                        ),
                        child: Text("Obsessive-Compulsive Disorder", style: TextStyle(fontSize: 20.0),)
                    ),
                  )
                ],
              ),
            ),
            Divider(),
            Container(
              margin: EdgeInsets.only(
                  top: 10.0,
                  left: 10.0
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(FontAwesomeIcons.pills, color: Colors.blueAccent,),
                  Expanded(
                    child: Container(
                        margin: EdgeInsets.only(
                            left: 20.0
                        ),
                        child: Text("Disruptive Mood Dysregulation Disorder", style: TextStyle(fontSize: 20.0),)
                    ),
                  )
                ],
              ),
            ),
            Divider(),
            Container(
              margin: EdgeInsets.only(
                  left: 10.0
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(FontAwesomeIcons.pills, color: Colors.blueAccent,),
                  Expanded(
                    child: Container(
                        margin: EdgeInsets.only(
                            left: 20.0
                        ),
                        child: Text("Major Depressive Disorder", style: TextStyle(fontSize: 20.0),)
                    ),
                  )
                ],
              ),
            ),
            Divider(),
            Container(
              margin: EdgeInsets.only(
                  left: 10.0
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(FontAwesomeIcons.pills, color: Colors.blueAccent,),
                  Expanded(
                    child: Container(
                        margin: EdgeInsets.only(
                            left: 20.0
                        ),
                        child: Text("Premenstrual Dysphoric Disorder", style: TextStyle(fontSize: 20.0),)
                    ),
                  )
                ],
              ),
            ),
            Divider(),
            Container(
              margin: EdgeInsets.only(
                  left: 10.0
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(FontAwesomeIcons.pills, color: Colors.blueAccent,),
                  Expanded(
                    child: Container(
                        margin: EdgeInsets.only(
                            left: 20.0
                        ),
                        child: Text("Cyclothymic Disorder", style: TextStyle(fontSize: 20.0),)
                    ),
                  )
                ],
              ),
            ),
            Divider(),
            Container(
              margin: EdgeInsets.only(
                  left: 10.0
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(FontAwesomeIcons.pills, color: Colors.blueAccent,),
                  Expanded(
                    child: Container(
                        margin: EdgeInsets.only(
                            left: 20.0
                        ),
                        child: Text("Panic Disorder", style: TextStyle(fontSize: 20.0),)
                    ),
                  )
                ],
              ),
            ),
            Divider(),
            Container(
              margin: EdgeInsets.only(
                  left: 10.0
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(FontAwesomeIcons.pills, color: Colors.blueAccent,),
                  Expanded(
                    child: Container(
                        margin: EdgeInsets.only(
                            left: 20.0
                        ),
                        child: Text("Agoraphobia", style: TextStyle(fontSize: 20.0),)
                    ),
                  )
                ],
              ),
            ),
            Divider(),
            Container(
              margin: EdgeInsets.only(
                  left: 10.0
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(FontAwesomeIcons.pills, color: Colors.blueAccent,),
                  Expanded(
                    child: Container(
                        margin: EdgeInsets.only(
                            left: 20.0
                        ),
                        child: Text("Obsessive-Compulsive Disorder", style: TextStyle(fontSize: 20.0),)
                    ),
                  )
                ],
              ),
            ),
          ],
        )
    );
  }
}