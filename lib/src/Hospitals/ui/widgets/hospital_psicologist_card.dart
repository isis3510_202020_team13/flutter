import 'package:flutter/cupertino.dart';

import 'hospital_card.dart';

class HospitalsPsicologistList extends StatelessWidget{
  @override

  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: ListView(
        children: [
          HospitalsCard("Psicologia Javier Lozano", "Psicology", 4.0, "M", "assets/img/h9.jpg", "stickgualteros@gmail.com", "3204117514"),
          HospitalsCard("Psicologia Andrea Medrano", "Psicology", 4.2, "M", "assets/img/h10.jpeg", "stickgualteros@gmail.com", "3204117514"),
        ],
      ),
    );
  }

}