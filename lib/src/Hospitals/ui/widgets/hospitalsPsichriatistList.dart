import 'package:flutter/cupertino.dart';

import 'hospital_card.dart';

class HospitalsPsichriatistList extends StatelessWidget{
  @override


  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: ListView(
        children: [
          HospitalsCard("Clinica Reina Sofia", "Psichriatry", 5.0, "M", "assets/img/h1.jpeg", "stickgualteros@gmail.com", "3204117514"),
          HospitalsCard("Clinica Monserrat", "Psichriatry", 5.0, "M", "assets/img/h2.jpeg", "stickgualteros@gmail.com", "3204117514"),
          HospitalsCard("Instituto Colombiano de la Salud Mental", "Psichriatry", 4.0, "M", "assets/img/h3.jpg", "stickgualteros@gmail.com", "3204117514"),
          HospitalsCard("CLinica del sistema nervioso", "Psichriatry", 4.0, "M", "assets/img/h5.png", "stickgualteros@gmail.com", "3204117514"),
          HospitalsCard("Renacer", "Psichriatry", 4.0, "M", "assets/img/h6.jpg", "stickgualteros@gmail.com", "3204117514"),
          HospitalsCard("Hospital Mederi", "Psichriatry", 4.0, "M", "assets/img/h7.jpg", "stickgualteros@gmail.com", "3204117514"),
          HospitalsCard("Clinica Santa fé", "Psichriatry", 4.0, "M", "assets/img/h8.jpg", "stickgualteros@gmail.com", "3204117514"),
        ],
      ),
    );
  }

}