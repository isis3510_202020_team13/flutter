import 'dart:math';

import 'package:f412/src/Scheduler/ui/widgets/schedule_card.dart';
import 'package:flutter/cupertino.dart';

class ScheduleCardList extends StatelessWidget{


  static Color color1 = new Color.fromRGBO(246,236,251,1.0);
  static Color color2 = new Color.fromRGBO(233,239,249, 1.0);
  static Color color3 = new Color.fromRGBO(223,215,207, 1.0);
  static Color color4 = new Color.fromRGBO(222, 253, 224,1.0);
  static Color color5 = new Color.fromRGBO(240, 222, 253, 1.0);
  static Color color6 = new Color.fromRGBO(253, 223, 223, 1.0);
  static Color color8 = new Color.fromRGBO(255,255,255, 1.0);
  static Color color9 = new Color.fromRGBO(201,201,255, 1.0);
  static Color color10 = new Color.fromRGBO(241,203,255, 1.0);



  List<Color> colourList = [color1, color2, color3, color4, color5, color6, color8, color9, color10];





  @override
  Widget build(BuildContext context) {
    // TODO: implement build

    String subject1 = "Software Architecture";
    String hour1 = "12:50 - 3:00";
    String priority1 = "Low";
    String description1 = "Sprint 6";
    Random randomizer1 = new Random();
    var randomIndex1 = randomizer1.nextInt(9);
    Color color1 = colourList[randomIndex1];

    String subject2 = "Biology";
    String hour2 = "4:50 - 8:00";
    String priority2 = "High";
    String description2 = "Deliverable 2";
    var randomIndex2 = randomizer1.nextInt(9);
    Color color2 = colourList[randomIndex2];

    String subject3 = "Differential Equations";
    String hour3 = "7:50 - 11:00";
    String priority3 = "Medium";
    String description3 = "Task 2";
    var randomIndex3 = randomizer1.nextInt(9);
    Color color3 = colourList[randomIndex3];


    String subject4 = "Phisics";
    String hour4 = "9:50 - 11:00";
    String priority4 = "Medium";
    String description4 = "Task 5";
    var randomIndex4 = randomizer1.nextInt(9);
    Color color4 = colourList[randomIndex4];

    var date1 = "Saturday, August 26";
    var date2 = "Sunday, August 27";


    return ListView(
      children: <Widget>[
        Container(
          alignment: Alignment.topRight,
          margin: EdgeInsets.only(
            right: 20.0,
            top: 20.0
          ),
            child: Text(
                date1,
                style: TextStyle(
                    fontSize: 15.0,
                    fontWeight: FontWeight.normal
                )
            )
        ),
        ScheduleCard(subject1, hour1, priority1, description1, color1),
        ScheduleCard(subject2, hour2, priority2, description2, color2),
        Container(
            alignment: Alignment.topRight,
            margin: EdgeInsets.only(
                right: 20.0,
                top: 20.0
            ),
            child: Text(
                date2,
                style: TextStyle(
                    fontSize: 15.0,
                    fontWeight: FontWeight.normal
                )
            )
        ),
        ScheduleCard(subject3, hour3, priority3, description3, color3),
        ScheduleCard(subject4, hour4, priority4, description4, color4),
        ScheduleCard(subject4, hour4, priority4, description4, color4),
      ],
    );
  }

}