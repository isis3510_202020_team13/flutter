import 'package:dropdownfield/dropdownfield.dart';
import 'package:f412/src/Scheduler/ui/screens/scheduler.dart';
import 'package:f412/src/Scheduler/ui/screens/sheduler_form_screen.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class SchedulerFormScreenState extends State<SchedulerFormScreen>{
  String tittle;
  DateTime date;
  TimeOfDay initialHour;
  TimeOfDay finalHour;
  String piority;
  String description;

  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();

  Widget _buildTittle(){
    return TextFormField(
      validator: (String value){
        if(value.isEmpty){
          return "Tittle is requiered";
        }
      },
      onSaved: (String value){
        tittle = value;
      },
    );
  }

  Widget _buildDate(){

    return InkWell(
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(
                top: 20.0,
                bottom: 20.0
            ),
            alignment: Alignment.topLeft,
            child: Icon(
                FontAwesomeIcons.calendar,
              color: Colors.blueAccent,
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              left: 10.0
            ),
            child: Text(
                "Click to set Date",
              style: TextStyle(
                color: Colors.grey

              ),
            ),
          )
        ],
      ),
      onTap: () {showDatePicker(context: context, initialDate: DateTime.now(), firstDate: DateTime(2001), lastDate: DateTime(2222));}
    );
  }

  Widget _buildInitialHour(){
    return InkWell(
      child: Row(
        children: [
          Container(
            margin: EdgeInsets.only(
              top: 20.0,
              bottom: 20.0
            ),
            alignment: Alignment.topLeft,
            child: Icon(
              FontAwesomeIcons.clock,
              color: Colors.blueAccent,
            ),
          ),
          Container(
            margin: EdgeInsets.only(
              left: 10.0
            ),
            child: Text(
              "Click to set starting time",
              style: TextStyle(
                color: Colors.grey
              ),
            ),
          )
        ],
      ),
      onTap: () {showTimePicker(context: context, initialTime: TimeOfDay.now());}
    );

  }

  Widget _buildFinalHour(){
    return InkWell(
        child: Row(
          children: [
            Container(
              margin: EdgeInsets.only(
                  top: 20.0,
                  bottom: 20.0
              ),
              alignment: Alignment.topLeft,
              child: Icon(
                FontAwesomeIcons.clock,
                color: Colors.blueAccent,
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                  left: 10.0
              ),
              child: Text(
                "Click to set finishing time",
                style: TextStyle(
                  color: Colors.grey

                ),
              ),
            )
          ],
        ),
        onTap: (){
          showTimePicker(context: context, initialTime: TimeOfDay.now());
        },
    );
  }



  Widget _buildPriority(){

    SizedBox(height: 20.0,);
    Widget dropDown = Container(
      margin: EdgeInsets.only(
        top: 10.0,
        bottom: 10.0
      ),
      child: DropDownField(
        controller: prioritiesSelected,
        hintText: "Select the priority of the event",
        enabled: true,
        items: priorities,
        onValueChanged: (value) {
          setState(() {
            selectPriority = value;
          });
        },
      ),
    );


    return dropDown;
  }

  Widget _buildDescription(){
    return TextFormField(
      validator: (String value){
        if(value.isEmpty){
          return "Description is requiered";
        }
      },
      onSaved: (String value){
        description = value;
      },
    );
  }


  @override
  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    final form = Container(
      height: MediaQuery.of(context).size.height * .60,
      child: Padding(
        padding: const EdgeInsets.all(9.0),
        child: ListView(
          children: [
            Row(
              children: [
                Text("Add Event", style: TextStyle(
                    fontSize: 25.0,
                    fontWeight: FontWeight.bold,
                    color: Colors.blueAccent
                ),
                ),
                Spacer(),
                IconButton(icon: Icon(Icons.cancel, color: Colors.blueAccent, size: 25.0,), onPressed: (){ Navigator.of(context).pop(); },)
              ],
            ),
            Text(
              "Tittle",
              style: TextStyle(
                  fontSize: 18.0,
                  fontWeight: FontWeight.normal
              ),
            ),
            Container(
              child: Form(child:
                  _buildTittle()
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                top: 10.0
              ),
              child: Text(
                "Date",
                style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.normal
                ),
              ),
            ),
            Container(
              child: Form(child:
              _buildDate()
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                top: 10.0
              ),
              child: Text(
                "Starts at",
                style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.normal
                ),
              ),
            ),
            Container(
              child: Form(child:
              _buildInitialHour()
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                top: 10.0
              ),
              child: Text(
                "Finish at",
                style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.normal
                ),
              ),
            ),
            Container(
              child: Form(child:
              _buildFinalHour()
              ),
            ),
            Container(
              margin: EdgeInsets.only(),
              child: Text(
                "Priority",
                style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.normal
                ),
              ),
            ),
            Container(
              child: Form(child:
              _buildPriority()
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                top:10.0
              ),
              child: Text(
                "Description",
                style: TextStyle(
                    fontSize: 18.0,
                    fontWeight: FontWeight.normal
                ),
              ),
            ),
            Container(
              child: Form(child:
              _buildDescription()
              ),
            ),
            Container(
              margin: EdgeInsets.only(
                top: 10.0
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  RaisedButton(
                    color: Colors.blueAccent,
                    child: Text("Submit", style: TextStyle(color: Colors.white, fontSize: 16 ),),
                    onPressed: () => {},
                  ),
                ],
              ),
            )
          ],
        ),
      ),
    );

    return form;
  }

}

String selectPriority = "";
final prioritiesSelected = TextEditingController();
List<String> priorities =[
  "High",
  "Medium",
  "Low"
];