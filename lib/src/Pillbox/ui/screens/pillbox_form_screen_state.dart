import 'package:f412/src/Medications/ui/screens/medication_pillbox.dart';
import 'package:f412/src/Pillbox/ui/screens/pillbox_form_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:number_display/number_display.dart';

class PillboxFormScreenState extends State<PillboxFormScreen>{
  @override

  MedicationPillbox medication;

  int counterDose = 0;

  void addToDose(){
    setState(() {
      counterDose++;
    });
  }

  void substractFromDose(){
    setState(() {
      if(counterDose != 0)
        counterDose--;
    });

  }

  int counterFrequency = 0;

  void addToFrequency(){
    setState(() {
      counterFrequency++;
    });
  }

  void substractFromFrequency(){
    setState(() {
      if(counterFrequency != 0)
        counterFrequency--;
    });

  }



  final GlobalKey<FormState> _formKey = GlobalKey<FormState>();


  Widget build(BuildContext context) {
    double width = MediaQuery.of(context).size.width;

    Widget _buildMedicationName(){
      return Container(
        width: width,
        child: Row(
          children: [
            Container(
              margin: EdgeInsets.only(
                top: 10.0,
                left: 10.0,
                right: 10.0
              ),
                child: Icon(FontAwesomeIcons.check, size: 20.0, color: Colors.deepOrangeAccent,)
            ),
            Flexible(
              child: Container(
                height: 35.0,
                margin: EdgeInsets.only(
                  top: 10.0,
                  left: 10.0,
                  right: 10.0
                ),
                child: TextFormField(
                  keyboardType: TextInputType.text,
                  decoration: InputDecoration(
                      labelText: "Medication name",
                    border: OutlineInputBorder()
                  ),
                  validator: (String value){
                    if(value.isEmpty){
                      return "Medication name is requiered";
                    }
                  },
                ),
              ),
            )
          ],

        ),
      );
    }

    Widget _buildDispenseAmount(){
      return Container(
        margin: EdgeInsets.only(
          bottom: 10.0
        ),
        width: width,
        child: Row(
          children: [
            Container(
                margin: EdgeInsets.only(
                    top: 10.0,
                    left: 10.0,
                    right: 10.0
                ),
                child: Icon(FontAwesomeIcons.tablets, size: 20.0, color: Colors.deepOrangeAccent,)
            ),
            Flexible(
              child: Container(
                height: 35.0,
                margin: EdgeInsets.only(
                    top: 10.0,
                    left: 10.0,
                    right: 10.0
                ),
                child: TextFormField(
                  keyboardType: TextInputType.number,
                  decoration: InputDecoration(
                      labelText: "Dispense amount",
                      border: OutlineInputBorder()
                  ),
                  validator: (String value){
                    if(value.isEmpty){
                      return "Dispense amount is requiered";
                    }
                  },
                ),
              ),
            )
          ],

        ),
      );
    }



    // bulding the personlized input widgets

    final counterWidgetDose = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(
            bottom: 10.0,
            left: 10.0
          ),
          child: Text(
            "Dose",
            style: TextStyle(

            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(
            left: 10.0
          ),
          child: Row(
            children: [
              InkWell(
                child: Container(
                  margin: EdgeInsets.only(
                      right: 10.0
                  ),
                  width: 30.0,
                  height: 30.0,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      shape: BoxShape.rectangle,
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            color: Colors.black38,
                            blurRadius: 15.0,
                            offset: Offset(0.0, 7.0)
                        )

                      ]

                  ),
                  child: Icon(FontAwesomeIcons.minus, color: Colors.deepOrangeAccent,),
                ),
                onTap: substractFromDose,
              ),
              Text(
                "$counterDose",
                style: TextStyle(
                    fontSize: 20.0
                ),
              ),
              InkWell(
                child: Container(
                  margin: EdgeInsets.only(
                      left: 10.0
                  ),
                  width: 30.0,
                  height: 30.0,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      shape: BoxShape.rectangle,
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            color: Colors.black38,
                            blurRadius: 15.0,
                            offset: Offset(0.0, 7.0)
                        )

                      ]
                  ),
                  child: Icon(FontAwesomeIcons.plus, color: Colors.deepOrangeAccent,),
                ),
                onTap: addToDose,
              )
            ],
          ),
        )
      ],
    );

    final counterWidgetFrequency = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(
            left: 10.0,
            bottom: 10.0
          ),
          child: Text(
            "Frequency",
            style: TextStyle(

            ),
          ),
        ),
        Container(
          margin: EdgeInsets.only(
            left: 10.0
          ),
          child: Row(
            children: [
              InkWell(
                child: Container(
                  margin: EdgeInsets.only(
                      right: 10.0
                  ),
                  width: 30.0,
                  height: 30.0,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      shape: BoxShape.rectangle,
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            color: Colors.black38,
                            blurRadius: 15.0,
                            offset: Offset(0.0, 7.0)
                        )

                      ]
                  ),
                  child: Icon(FontAwesomeIcons.minus, color: Colors.deepOrangeAccent,),
                ),
                onTap: substractFromFrequency,
              ),
              Text(
                "$counterFrequency",
                style: TextStyle(
                    fontSize: 20.0
                ),
              ),
              InkWell(
                child: Container(
                  margin: EdgeInsets.only(
                      left: 10.0
                  ),
                  width: 30.0,
                  height: 30.0,
                  decoration: BoxDecoration(
                      color: Colors.white,
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                      shape: BoxShape.rectangle,
                      boxShadow: <BoxShadow>[
                        BoxShadow(
                            color: Colors.black38,
                            blurRadius: 15.0,
                            offset: Offset(0.0, 7.0)
                        )

                      ]
                  ),
                  child: Icon(FontAwesomeIcons.plus, color: Colors.deepOrangeAccent,),
                ),
                onTap: addToFrequency,
              )
            ],
          ),
        )
      ],
    );
    String dropdownValue = "Mmg";
    final unitSelector = Container(
      margin: EdgeInsets.only(
        top: 10.0,
        right: 10.0
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Unit",
            style: TextStyle(

            ),
          ),
          DropdownButton<String>(
            value: dropdownValue,
            onChanged: (String newValue) {
              setState(() {
                dropdownValue = newValue;
              });
            },
            items: ["Mmg", "Mg", "G"].map<DropdownMenuItem<String>>((String value){
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }
            ).toList(),
          )
        ],
      ),
    );

    String dropdownValue2 = "Day";
    final frequencyPerSelector = Container(
      margin: EdgeInsets.only(
        top: 10.0
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Text(
            "Per",
            style: TextStyle(

            ),
          ),
          DropdownButton<String>(
            value: dropdownValue2,
            onChanged: (String newValue) {
              setState(() {
                dropdownValue2 = newValue;
              });
            },
            items: ["Day", "Week", "Month"].map<DropdownMenuItem<String>>((String value){
              return DropdownMenuItem<String>(
                value: value,
                child: Text(value),
              );
            }
            ).toList(),
          )
        ],
      ),
    );



    // the days for the personalized day selector

    final monday = Container(
      margin: EdgeInsets.only(
          left: 10.0,
          right: 10.0
      ),
      width: 40.0 ,
      height: 50.0,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.all(Radius.circular(13.0)),
        shape: BoxShape.rectangle,
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Colors.black38,
            blurRadius: 15.0,
            offset: Offset(0.0, 7.0)
          )

        ]
      ),
      child: Container(
        alignment: Alignment.center,
        child: Text(
          "M",
          style: TextStyle(
            color: Colors.deepOrangeAccent,
            fontWeight: FontWeight.normal,
            fontSize: 20.0
          ),
        ),
      ),

    );

    final tuesday = Container(
      margin: EdgeInsets.only(
          left: 10.0,
          right: 10.0
      ),
      width: 40.0 ,
      height: 50.0,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(13.0)),
          shape: BoxShape.rectangle,
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.black38,
                blurRadius: 15.0,
                offset: Offset(0.0, 7.0)
            )

          ]
      ),
      child: Container(
        alignment: Alignment.center,
        child: Text(
          "T",
          style: TextStyle(
              color: Colors.deepOrangeAccent,
              fontWeight: FontWeight.normal,
              fontSize: 20.0
          ),
        ),
      ),

    );

    final wednesday = Container(
      margin: EdgeInsets.only(
          left: 10.0,
          right: 10.0
      ),
      width: 40.0 ,
      height: 50.0,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(13.0)),
          shape: BoxShape.rectangle,
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.black38,
                blurRadius: 15.0,
                offset: Offset(0.0, 7.0)
            )

          ]
      ),
      child: Container(
        alignment: Alignment.center,
        child: Text(
          "W",
          style: TextStyle(
              color: Colors.deepOrangeAccent,
              fontWeight: FontWeight.normal,
              fontSize: 20.0
          ),
        ),
      ),

    );

    final thursday = Container(
      margin: EdgeInsets.only(
          left: 10.0,
          right: 10.0
      ),
      width: 40.0 ,
      height: 50.0,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(13.0)),
          shape: BoxShape.rectangle,
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.black38,
                blurRadius: 15.0,
                offset: Offset(0.0, 7.0)
            )

          ]
      ),
      child: Container(
        alignment: Alignment.center,
        child: Text(
          "T",
          style: TextStyle(
              color: Colors.deepOrangeAccent,
              fontWeight: FontWeight.normal,
              fontSize: 20.0
          ),
        ),
      ),

    );

    final friday = Container(
      margin: EdgeInsets.only(
          left: 10.0,
          right: 10.0
      ),
      width: 40.0 ,
      height: 50.0,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(13.0)),
          shape: BoxShape.rectangle,
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.black38,
                blurRadius: 15.0,
                offset: Offset(0.0, 7.0)
            )

          ]
      ),
      child: Container(
        alignment: Alignment.center,
        child: Text(
          "F",
          style: TextStyle(
              color: Colors.deepOrangeAccent,
              fontWeight: FontWeight.normal,
              fontSize: 20.0
          ),
        ),
      ),

    );

    final saturday = Container(
      margin: EdgeInsets.only(
          left: 10.0,
          right: 10.0
      ),
      width: 40.0 ,
      height: 50.0,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(13.0)),
          shape: BoxShape.rectangle,
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.black38,
                blurRadius: 15.0,
                offset: Offset(0.0, 7.0)
            )

          ]
      ),
      child: Container(
        alignment: Alignment.center,
        child: Text(
          "S",
          style: TextStyle(
              color: Colors.deepOrangeAccent,
              fontWeight: FontWeight.normal,
              fontSize: 20.0
          ),
        ),
      ),

    );

    final sunday = Container(
      margin: EdgeInsets.only(
        left: 10.0,
        right: 10.0
      ),
      width: 40.0 ,
      height: 50.0,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.all(Radius.circular(13.0)),
          shape: BoxShape.rectangle,
          boxShadow: <BoxShadow>[
            BoxShadow(
                color: Colors.black38,
                blurRadius: 15.0,
                offset: Offset(0.0, 7.0)
            )

          ]
      ),
      child: Container(
        alignment: Alignment.center,
        child: Text(
          "S",
          style: TextStyle(
              color: Colors.deepOrangeAccent,
              fontWeight: FontWeight.normal,
              fontSize: 20.0
          ),
        ),
      ),

    );

    //the personalized day selector
    //Personalized day selector

    final daySelector = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Container(
          margin: EdgeInsets.only(
            left: 10.0,
            bottom: 10.0
          ),
          child: Text(
              "Days of the week",
            style: TextStyle(

            ),
          ),
        ),
        Container(
          width: width,
          child: Row(
            children: [
              Expanded(child: monday),
              Expanded(child: tuesday),
              Expanded(child: wednesday),
              Expanded(child: thursday),
              Expanded(child: friday),
              Expanded(child: saturday),
              Expanded(child: sunday)
            ],
          ),
        )
      ],
    );

    final header =  Container(
      margin: EdgeInsets.only(
        left: 10.0,
      ),
      child: Row(
        children: [
          Text("Add new medication", style: TextStyle(
              fontSize: 25.0,
              fontWeight: FontWeight.bold,
              color: Colors.deepOrangeAccent
          ),
          ),
          Spacer(),
          IconButton(icon: Icon(Icons.cancel, color: Colors.deepOrangeAccent, size: 25.0,), onPressed: (){ Navigator.of(context).pop(); },)
        ],
      ),
    );

    final submitButton = Container(
      margin: EdgeInsets.only(
        top: 10.0
      ),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.center,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          RaisedButton(
            color: Colors.white,
            child: Text("Submit", style: TextStyle(color: Colors.deepOrangeAccent, fontSize: 16 ),),
            onPressed: () => {},
          ),
        ],
      ),
    );

    // TODO: implement build
    return Container(
      height: MediaQuery.of(context).size.height * .60,
        child: ListView(
          children: [
            header,
            _buildMedicationName(),
            Row(
              children: [
                counterWidgetDose,
                Spacer(),
                unitSelector,
              ],
            ),
            Row(
              children: [
                counterWidgetFrequency,
                Spacer(),
                frequencyPerSelector,
              ],
            ),
            _buildDispenseAmount(),
            daySelector,
            submitButton
          ],
        )

    );
  }


}