import 'package:f412/src/Pillbox/ui/screens/pillbox.dart';
import 'package:f412/src/Pillbox/ui/screens/pillbox_form_screen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:table_calendar/table_calendar.dart';

import '../../../Medications/ui/widgets/medication_pillbox_list.dart';
class PillboxState extends State<Pillbox>{

  CalendarController _controller;

  void initState(){
    super.initState();
    _controller = CalendarController();
  }


  @override
  Widget build(BuildContext context) {

    void _addEvent(context){
      showModalBottomSheet(context: context, builder: (BuildContext bc){
        return PillboxFormScreen();
      });
    }

    double width = MediaQuery.of(context).size.width;
    double height = MediaQuery.of(context).size.width;

    final calendar = TableCalendar(
      calendarStyle: CalendarStyle(
        todayColor: Colors.orange,
        selectedColor: Colors.deepOrangeAccent,
        markersColor: Colors.deepOrangeAccent
      ),
      calendarController: _controller,

    );



    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        actions: <Widget> [

        ],
        backgroundColor: Colors.white,
        iconTheme: IconThemeData(
          color: Colors.deepOrangeAccent,
        ),
        title: Text(
          "Pillbox",
          style: TextStyle(
            color: Colors.deepOrangeAccent,
          ),
        ),
      ),
      body: Stack(
        children: [
          Container(
              child: calendar
          ),
          Container(
            decoration: BoxDecoration(
              color: Colors.white
            ),
            margin: EdgeInsets.only(
              top: height*0.9
            ),
              child: MedicationPillboxList()
          )
        ],
      ),
        floatingActionButton: FloatingActionButton(
          onPressed: () {
            _addEvent(context);
            // Add your onPressed code here!
          },
          backgroundColor: Colors.deepOrangeAccent,
          child: Icon(FontAwesomeIcons.plus),
        )
    );
  }

}