import 'package:f412/src/Doctors/ui/widgets/doctors_psichriatrist_list.dart';
import 'package:f412/src/Doctors/ui/widgets/doctors_psicologist_list.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';

class Doctors extends StatelessWidget{

  PageController _pageController = PageController();


  @override
  Widget build(BuildContext context) {
    int currentIndex = 0;

    final psichriatyButton = Material(
      child: InkWell(
        child: Container(
          margin: EdgeInsets.only(
              left: 10.0,
              right: 10.0
          ),
          child: Column(
            children: [
              Container(
                height: 80.0,
                width: 80.0,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    shape: BoxShape.rectangle,
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: Colors.black38,
                          blurRadius: 15.0,
                          offset: Offset(0.0, 7.0)
                      )
                    ]
                ),
                child: Icon(FontAwesomeIcons.stethoscope, color: Colors.blueAccent, size: 30.0,),
              ),
              Container(
                margin: EdgeInsets.only(
                    top: 10.0
                ),
                child: Text(
                  "Psichiatry",
                  style: TextStyle(

                  ),
                  textAlign: TextAlign.center,
                ),
              )
            ],
          ),
        ),
        onTap: () { currentIndex = 0;},
      ),
    );

    final psicologyButton = Material(
      child: InkWell(
        child: Container(
          margin: EdgeInsets.only(
            left: 10.0,
            right: 10.0,
          ),
          child: Column(
            children: [
              Container(
                height: 80.0,
                width: 80.0,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    shape: BoxShape.rectangle,
                    boxShadow: <BoxShadow>[
                      BoxShadow(
                          color: Colors.black38,
                          blurRadius: 15.0,
                          offset: Offset(0.0, 7.0)
                      )
                    ]
                ),
                child: Icon(FontAwesomeIcons.penAlt, color: Colors.blueAccent, size: 30.0,),
              ),
              Container(
                margin: EdgeInsets.only(
                    top: 10.0
                ),
                child: Text(
                  "Psicology",
                  style: TextStyle(

                  ),
                  textAlign: TextAlign.center,
                ),
              )
            ],
          ),
        ),
        onTap: () { currentIndex = 1;},
      ),
    );

    final categoryButtonSet = Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Container(
            margin: EdgeInsets.only(
              left: 10.0,
              right: 10.0,
              top: 10.0,
              bottom: 20.0
            ),
            child: Text(
              "Categories",
              style: TextStyle(
                fontSize: 18.0,
                color: Colors.blueAccent
              ),
            ),
          ),
          Row(
            children: [
              psichriatyButton,
              psicologyButton
            ],
          )
        ],
      ),
    );


    return  Scaffold(
        backgroundColor: Colors.white,
        appBar: AppBar(
          actions: <Widget> [

          ],
          backgroundColor: Colors.white,
          iconTheme: IconThemeData(
            color: Colors.blueAccent,
          ),
          title: Text(
            "Doctors",
            style: TextStyle(
              color: Colors.blueAccent,
            ),
          ),
        ),
      body: Stack(
        children: [
          Container(
            margin: EdgeInsets.only(
              top: 170.0
            ),
              child:
              PageView(
                controller: _pageController,
                  onPageChanged: (page){
                  },
                  children: [
                    SafeArea(
                        child: DoctorsPsichriatistList()
                    ),
                    SafeArea(
                        child: DoctorsPsicologistList()
                    )
                  ],
              )
          ),
          categoryButtonSet,
        ],
      )
    );
  }

}