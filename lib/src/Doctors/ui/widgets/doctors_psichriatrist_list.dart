import 'package:flutter/cupertino.dart';

import 'doctors_card.dart';

class DoctorsPsichriatistList extends StatelessWidget{
  @override


  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: ListView(
        children: [
          DoctorsCard("Andres Rodriguez", "Psichriatry", 5.0, "M", "assets/img/Andres.jpeg", "stickgualteros@gmail.com", "3204117514"),
          DoctorsCard("Stick Gualteros", "Psichriatry", 5.0, "M", "assets/img/Stick.jpeg", "stickgualteros@gmail.com", "3204117514"),
          DoctorsCard("Shawn Murphy", "Psichriatry", 4.0, "M", "assets/img/Doc6.jpg", "stickgualteros@gmail.com", "3204117514"),
          DoctorsCard("Pedro Sanchez", "Psichriatry", 4.0, "M", "assets/img/Doc1.jpeg", "stickgualteros@gmail.com", "3204117514"),
          DoctorsCard("Victor Encina", "Psichriatry", 4.0, "M", "assets/img/Doc2.jpeg", "stickgualteros@gmail.com", "3204117514"),
          DoctorsCard("Martha Ramirez", "Psichriatry", 4.0, "M", "assets/img/Doc3.jpg", "stickgualteros@gmail.com", "3204117514"),
          DoctorsCard("Mike Wasouski", "Psichriatry", 4.0, "M", "assets/img/Doc4.jpg", "stickgualteros@gmail.com", "3204117514"),
        ],
      ),
    );
  }

}