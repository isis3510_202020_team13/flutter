import 'package:flutter/cupertino.dart';

import 'doctors_card.dart';


class DoctorsPsicologistList extends StatelessWidget{
  @override

  Widget build(BuildContext context) {
    // TODO: implement build
    return Container(
      child: ListView(
        children: [
          DoctorsCard("Javier Lozano", "Psicology", 4.0, "M", "assets/img/Psi1.jpg", "stickgualteros@gmail.com", "3204117514"),
          DoctorsCard("Andrea Medrano", "Psicology", 4.2, "M", "assets/img/Psi2.jpg", "stickgualteros@gmail.com", "3204117514"),
        ],
      ),
    );
  }

}